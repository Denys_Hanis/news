//
//  File.swift
//  News
//
//  Created by Denis Ganis on 3/28/18.
//  Copyright © 2018 Denis Ganis. All rights reserved.
//

import Foundation
import UIKit

extension UIImageView {
  
  func downloadImage(from url: String) {
    
    let urlRequest = URLRequest(url: URL(string: url)!)
    
    let task = URLSession.shared.dataTask(with: urlRequest) { (data,response,error) in
      
      if error != nil {
        print(error!)
        return
      }
      DispatchQueue.main.sync {
        self.image = UIImage(data: data!)
      }
    }
    task.resume()
  }
}

//
//  FilterDataProvider.swift
//  News
//
//  Created by Denis Ganis on 3/28/18.
//  Copyright © 2018 Denis Ganis. All rights reserved.
//

import UIKit

class FilterItem {
  func urlStringForItem(item: String) -> String {
    return "https://newsapi.org/v2/top-headlines?\(item)&apiKey=\(apiKey)"
  }
}

class Country: FilterItem {
  var name = String()
  var countryCode = String()
  var url: String {
    return urlStringForItem(item: "country=\(countryCode)")
  }
}

class Category: FilterItem {
  var name = String()
  var url: String {
    return urlStringForItem(item: "category=\(name)")
  }
}

class Source: FilterItem {
  var name = String()
  var sourceCode = String()
  var url: String {
    return urlStringForItem(item: "sources=\(sourceCode)")
  }
}

class FilterDataProvider {//TODO: three parsed arrays with above objects
  static func parse() -> [Any] {
    var countries = [Country]()
    var categories = [Category]()
    var sources = [Source]()
    
    if let path = Bundle.main.url(forResource: "filterData", withExtension: "json") {
      do {
        var dataArray = [Any]()
         
        let data = try Data(contentsOf: path)
        let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
        if let country = json!["country"] as? [String: Any] {
          var counties = [String]()
          for (property, list) in country {
            
          }
          
        }
      } catch {
        // handle error
      }
    }
    return ["pes"]
  }
}

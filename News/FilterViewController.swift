//
//  FilterViewController.swift
//  News
//
//  Created by Denis Ganis on 3/28/18.
//  Copyright © 2018 Denis Ganis. All rights reserved.
//

import UIKit

protocol DataReceiver: class {
  func dataReceiverDidSelected(filter: FilterName, value: String)
}

enum FilterName: String {
  case Category = "category"
  case Country = "country"
  case Sources = "sources"
}
class FilterViewController: UIViewController {
  
  var countryCodes = ["ar", "au", "at", "be", "br"]
  var categoriesNames = ["business", "entertainment", "general", "health", "science", "sports", "technology"]
  var sourcesCodes = ["abc-news", "afterposten", "al-jazeera-english", "bbc-news", "bloomberg"]
  
  weak var delegate: DataReceiver?
  
  override func viewDidLoad() {
    super.viewDidLoad()
  }
  
  func getItemForIndexPath(_ indexPath: IndexPath) -> String {
    switch indexPath.section {
    case 0:
      return countryCodes[indexPath.row]
    case 1:
      return categoriesNames[indexPath.row]
    default:
      return sourcesCodes[indexPath.row]
    }
  }
  
  //MARK: Actions
  
  @IBAction func showAllArticlesTapped(_ sender: UIButton) {
    //TODO: implement
    navigationController?.popViewController(animated: true)
  }
}

// MARK: - UITableViewDelegate

extension FilterViewController: UITableViewDelegate {
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    switch indexPath.section {
    case 0:
      delegate?.dataReceiverDidSelected(filter: .Country, value: countryCodes[indexPath.row])
    case 1:
      delegate?.dataReceiverDidSelected(filter: .Category, value: categoriesNames[indexPath.row])
    default:
      delegate?.dataReceiverDidSelected(filter: .Sources, value: sourcesCodes[indexPath.row])
    }
    navigationController?.popViewController(animated: true)
  }
}

// MARK: - UITableViewDataSource

extension FilterViewController: UITableViewDataSource {
  
  func numberOfSections(in tableView: UITableView) -> Int {
    return 3
  }
  
  func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
    let sectionHeaders = ["Category", "Country", "Sources"]
    return sectionHeaders[section]
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    switch section {
    case 0:
      return countryCodes.count
    case 1:
      return categoriesNames.count
    case 2:
      return sourcesCodes.count
    default:
      return 0
    }
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    let cell = tableView.dequeueReusableCell(withIdentifier: "filterItemCell", for: indexPath) as! FilterItemCell
    let item = getItemForIndexPath(indexPath)
    cell.filterLabel.text = item
    return cell
  }
}

//
//  FilterItemCell.swift
//  News
//
//  Created by Denis Ganis on 3/28/18.
//  Copyright © 2018 Denis Ganis. All rights reserved.
//

import UIKit

class FilterItemCell: UITableViewCell {

    @IBOutlet weak var filterLabel: UILabel!
    @IBOutlet weak var checkbox: UIButton!
    
    @IBAction func onCheckboxTapped(_ sender: UIButton) {
        checkbox.isSelected = !checkbox.isSelected
    }
}

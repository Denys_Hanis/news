//
//  NetworkManager.swift
//  News
//
//  Created by Denis Ganis on 3/28/18.
//  Copyright © 2018 Denis Ganis. All rights reserved.
//

import Foundation
let apiKey = "09388f522fa841fd93f9a865f106f24c"
class NetworkManager {
  
  static func fetchArticles(page: Int, search: String?, filter: String?, onSuccess: @escaping ([Article])->Void , onError: @escaping (String)->Void) {
    let lsearch = search ?? ""
    var urlString = "https://newsapi.org/v2/everything?domains=wsj.com&q=\(lsearch)&apiKey=\(apiKey)&page=\(page)&pageSize=5"

    if filter != nil {
      urlString = "https://newsapi.org/v2/top-headlines?q=\(lsearch)&\(filter!)&apiKey=\(apiKey)&page=\(page)&pageSize=5"
    }
    let urlRequest = URLRequest(url: URL(string: urlString)!)
    
    let task = URLSession.shared.dataTask(with: urlRequest) {
      (data,response,error) in
    
      if error != nil {
        print(error!)
        onError(error!.localizedDescription)
        return
      }
      
      do {
        let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as! [String : AnyObject]
        if let httpResponse = response as? HTTPURLResponse {
          if httpResponse.statusCode != 200 {
            if let errorMessage = json["message"] as? String {
              onError(errorMessage)
            } else {
              onError("Error")
            }
            return
          }
        }
        if let articlesFromJson = json["articles"] as? [[String : AnyObject]] {
          var articles = [Article]()
          for articleFromJson in articlesFromJson {
            
            if let title       = articleFromJson["title"] as? String,
              let authorName  = articleFromJson["author"] as? String,
              let descript    = articleFromJson["description"] as? String,
              let url         = articleFromJson["url"] as? String,
              let urlToImage  = articleFromJson["urlToImage"] as? String,
              let sourse      = articleFromJson["source"] as? [String : AnyObject] {
              
              if let name = sourse["name"] as? String {
                let article = Article(headline: title, desc: descript, author: authorName, url: url, imageUrl: urlToImage, sourseName: name)
                articles.append(article)
              }
            }
          }
          onSuccess(articles)
        }
        
      } catch let error {
        print(error)
        onError(error.localizedDescription)
      }
    }
    task.resume()
    
  }
}

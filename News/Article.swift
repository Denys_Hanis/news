//
//  Article.swift
//  News
//
//  Created by Denis Ganis on 3/28/18.
//  Copyright © 2018 Denis Ganis. All rights reserved.
//

import UIKit

class Article: NSObject {
  
  var headline: String
  var desc: String
  var author: String
  var url: String
  var imageUrl: String
  var sourseName: String
  
  init(headline: String, desc: String, author: String, url: String, imageUrl: String, sourseName: String) {
    self.headline = headline
    self.desc = desc
    self.author = author
    self.url = url
    self.imageUrl = imageUrl
    self.sourseName = sourseName
  }
}

//
//  ViewController.swift
//  News
//
//  Created by Denis Ganis on 3/28/18.
//  Copyright © 2018 Denis Ganis. All rights reserved.
//

import UIKit
import SafariServices

class ViewController: UIViewController {
  
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var searchBar: UISearchBar!
  
  var articles = [Article]()
  var currentPage = 1
  var searchString: String?
  var currentFilter: String?
  var refresher: UIRefreshControl!
  
  // MARK: - LifeCycle
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    refresher = UIRefreshControl()
    refresher.addTarget(self, action: #selector(ViewController.refresh), for: UIControlEvents.valueChanged)
    tableView.addSubview(refresher)
    
    fetchArticles(page: currentPage, search: searchString, filter: currentFilter, refresh: false)
    searchBar.delegate = self
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
  }
  
  // MARK: - Private Methods
  
  func fetchArticles(page: Int, search: String?, filter: String?, refresh: Bool) {
    NetworkManager.fetchArticles(page: page, search: search, filter: filter, onSuccess: { [weak self] (articles) in
      guard let strongSelf = self else { return }
      
      if refresh {
        strongSelf.articles = articles
        DispatchQueue.main.async {
          strongSelf.refresher.endRefreshing()
        }
      } else {
        strongSelf.articles = strongSelf.articles + articles
      }
      DispatchQueue.main.async {
        strongSelf.tableView.reloadData()
      }
      }, onError: {(error) in

        self.createAlert(title: "Error", message: error)
        if refresh {
          DispatchQueue.main.async {
            self.refresher.endRefreshing()
          }
        }
    })
  }
  
  @objc private func refresh() {
    currentPage = 1
    fetchArticles(page: currentPage, search: searchString, filter: currentFilter, refresh: true)
  }
  
  private func createAlert(title: String, message: String) {
    
    let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (action) in
      alert.dismiss(animated: true, completion: nil)
    }))
    self.present(alert, animated: true, completion: nil)
  }
  
  //MARK: Actions
  
  @IBAction func filterButtonTapped(_ sender: UIButton) {
    let filterViewController = self.storyboard?.instantiateViewController(withIdentifier: "filterVC") as! FilterViewController
    filterViewController.delegate = self
    self.navigationController?.pushViewController(filterViewController, animated: true)
  }
}

// MARK: - UITableViewDelegate

extension ViewController: UITableViewDelegate {
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let article = articles[indexPath.row]
    if let url = URL(string: article.url) {
      let safariVC = SFSafariViewController(url: url)
      self.present(safariVC, animated: true, completion: nil)
    }
  }
  
  func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    
    let lastItem = articles.count - 1
    if indexPath.row == lastItem {
      
      if articles.count > 1 {
        currentPage += 1
//        fetchArticles(page: currentPage, search: searchString, filter: currentFilter, refresh: false)
      }
    }
  }
}

// MARK: - UITableViewDataSource

extension ViewController: UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return self.articles.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "newsCell", for: indexPath) as! NewsCell
    let article = self.articles[indexPath.row]
    cell.titleLabel.text = article.headline
    cell.descLabel.text = article.desc
    cell.authorLabel.text = article.author
    cell.imgView.downloadImage(from: (article.imageUrl))
    cell.sourseNameLabel.text = article.sourseName
    
    return cell
  }
}

extension ViewController: DataReceiver {
  func dataReceiverDidSelected(filter: FilterName, value: String) {
    currentFilter = filter.rawValue + "=" + value
    articles = [Article]()
    fetchArticles(page: 1, search: searchString, filter: currentFilter, refresh: false)
  }
}

extension ViewController: UISearchBarDelegate {

  func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
    articles = [Article]()
    searchString = searchBar.text
    fetchArticles(page: 1, search: searchString, filter: currentFilter, refresh: false)
  }
}
